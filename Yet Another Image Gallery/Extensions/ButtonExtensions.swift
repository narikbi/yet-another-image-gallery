//
//  ButtonExtensions.swift
//  Utdannet
//
//  Created by Narikbi on 10/11/16.
//  Copyright © 2016 All rights reserved.
//

import UIKit

extension UIButton {

    // MARK: Actions

    func tap(target:Any?, selector:Selector) {
        self.addTarget(target, action: selector, for: .touchUpInside)
    }

    // MARK: Creation

    static func button(icon:String!, xInset:CGFloat = 0, yInset:CGFloat = 0) -> UIButton {
        return self.button(image: UIImage.init(named: icon), xInset: xInset, yInset: yInset)
    }

    static func button(icon:String!, sideInset:CGFloat = 0) -> UIButton {
        return self.button(image: UIImage.init(named: icon), xInset: sideInset, yInset: sideInset)
    }

    static func button(image:UIImage!, sideInset:CGFloat = 0) -> UIButton {
        return self.button(image: image, xInset: sideInset, yInset: sideInset)
    }

    static func button(icon:String!, top:CGFloat = 0, left:CGFloat = 0, bottom:CGFloat = 0, right:CGFloat = 0, highlightedIcon:String? = nil) -> UIButton {
        var highlightedImage : UIImage?
        if let icon = highlightedIcon {
            highlightedImage = UIImage(named: icon)
        }
        return button(image: UIImage.init(named: icon), top: top, left: left, bottom: bottom, right: right, highlightedImage:highlightedImage)
    }

    static func button(image:UIImage!, xInset:CGFloat = 0, yInset:CGFloat = 0) -> UIButton {
        return button(image: image, top: yInset, left: xInset, bottom: yInset, right: xInset)
    }

    static func button(image:UIImage!, top:CGFloat = 0, left:CGFloat = 0, bottom:CGFloat = 0, right:CGFloat = 0, highlightedImage:UIImage? = nil) -> UIButton {
        let button = UIButton()

        button.setImage(image, for: .normal)
        if let highlighted = highlightedImage {
            button.setImage(highlighted, for: .highlighted)
        } else {
            button.showsTouchWhenHighlighted = true
        }

        button.contentEdgeInsets = UIEdgeInsetsMake(top, left, bottom, right)
        button.frame.size.width = left + right + image.size.width
        button.frame.size.height = bottom + top + image.size.height
        return button
    }

    // MARK: Bounds

    func setInsets(horizontal:CGFloat = 0, vertical:CGFloat = 0) {
        var insets = self.contentEdgeInsets
        insets.left = horizontal
        insets.right = horizontal
        insets.top = vertical
        insets.bottom = vertical
        self.contentEdgeInsets = insets
    }
    
    // MARK: Attributes

    func setLetterSpacing(title:String!, value:CGFloat) {
        self.setAttributedTitle(NSAttributedString(string: title, attributes: [NSKernAttributeName: value]), for: .normal)
    }
}
