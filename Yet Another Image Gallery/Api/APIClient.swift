//
//  APIClient.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


/// APIClient is Base class for sending all RESTful requests through the app,
/// such as GET, POST, DELETE, UPDATE,
/// but since we're dealing only with GET requests
/// APIClient sends only this method
///

typealias Success = (_ data: [String:Any]?) -> Void
typealias Failure = (_ message:String) -> Void

public var baseURL: String { return "https://api.flickr.com/services/feeds/" }


class APIClient {

/// Sends GET method requests
/// - parameter path - A path that is to be requested
/// - parameter params - A dictionary that represents all `GET` parameters
/// - parameter success - A block to be executed when request becomes succesfull
/// - parameter failure - A block to be executed when request failed

    static func GET(path:String, params:[String:Any], success:@escaping Success, failure:@escaping Failure) {
        
        Alamofire.request(baseURL+path, method: .get, parameters: params).responseJSON { (response) in

            print(response)
            
            switch response.result {
            case .success:
                guard let data = response.result.value as? [String:Any] else { return }
                success(data)
                
            case let .failure(error):
                UIApplication.shared.keyWindow?.rootViewController?.alert(title: "Error", message: error.localizedDescription)
                failure(error.localizedDescription);
            }
        }
        
    }
    
}




