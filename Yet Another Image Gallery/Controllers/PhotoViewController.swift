//
//  PhotoViewController.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 22.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit
import TagListView
import SKPhotoBrowser
import UIAlertController_Blocks
import MessageUI
import SDWebImage

/// PhotoViewController is UIViewController subclass that shows all the detailed metadata
/// about each photo

class PhotoViewController: UIViewController, TagListViewDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var dateLabel: UILabel!
  
    // `Photo` object that is passed FeedVC
    var photo:Photo = Photo()

    // MARK: init
    static func instantiate() -> PhotoViewController {
        return UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
    }
    
    
    // MARK: Actions
    
    @IBAction func imageTapped(_ sender: Any) {
        
        // showing photo in separate SKPhotoBrowser class
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImageURL(self.photo.fullsizePhoto())
        photo.shouldCachePhotoURLImage = true
        images.append(photo)
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        self.present(browser, animated: true, completion: {})
        
    }
    
    func shareTapped() {
        UIAlertController.showActionSheet(in: self, withTitle: "Action", message: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Save photo", "Send by email", "Open photo in Safari"], popoverPresentationControllerBlock: nil) { [unowned self] (controller, action, buttonIndex) in
            
            if buttonIndex == controller.firstOtherButtonIndex {
                self.savePhoto()
            } else if buttonIndex == controller.firstOtherButtonIndex+1 {
                self.sendByEmail()
            } else if buttonIndex == controller.firstOtherButtonIndex+2 {
                self.openInBrowser()
            }
            
        }
    }
    
    
    //  Adds a photo to the saved photos album
    // MARK: Saving photo
    
    func savePhoto() {
        UIImageWriteToSavedPhotosAlbum((self.imageButton.imageView?.image)!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    /// - parameter image - image to be saved
    /// - parameter error - NSError subclass checking for saving success
    /// - parameters contextInfo - A raw pointer for accessing untyped data.
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            self.showSV(with: error.localizedDescription)
        } else {
            self.successSV(message: "Successfully saved")
        }
    }
    
    /// Sharing a photo by email using MFMailComposeViewController class
    // MARK: Sending email
    
    func sendByEmail() {
        let composer = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            composer.mailComposeDelegate = self
            composer.setSubject("Photo from Flickr")
            
            if let image = self.imageButton.imageView?.image {
                let data = UIImageJPEGRepresentation(image, 1.0)
                composer.addAttachmentData(data!, mimeType: "image/jpg", fileName: "image")
            }
            
            present(composer, animated: true, completion: nil)
        }
    }
    
    // MARK: Open photo in browser
    
    func openInBrowser() {
        UIApplication.shared.openURL(URL.init(string: photo.fullsizePhoto())!)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled:
            break
        case .saved:
            self.successSV(message: "Successfully saved")
            break
        case .sent:
            self.successSV(message: "Successfully sent")
            break
        case .failed:
            self.showSV(with: (error?.localizedDescription)!)
            break
        }

        controller.dismissController()
        
    }

    
    // MARK: TagListViewDelegate
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
        // when tag is selected posting Notification with tag info and getting back
        NotificationsHelper.post(name: kNotificationTagSelected, object: ["tag" : title])
        self.navigationController?.popViewController(animated: true)
    }

    /// Fulfilling all the UI object with data
    // MARK: configUI
    func configUI() {
        self.title = "Info"
        self.setNavigationBack()
        
        self.imageButton.sd_setImage(with: URL(string: self.photo.fullsizePhoto()), for: .normal)
        self.titleLabel.text = self.photo.title
        self.authorLabel.text = self.photo.formattedUsername()
        self.dateLabel.text = self.photo.formattedDateTaken()
        self.imageButton.imageView?.contentMode = .scaleAspectFit
      
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(PhotoViewController.shareTapped))
        self.navigationItem.rightBarButtonItem = shareButton
        
        // castomizing tagView class
        self.tagView.cornerRadius = 0
        self.tagView.borderColor = UIColor.black
        self.tagView.borderWidth = 1.0
        self.tagView.textColor = UIColor.black
        self.tagView.tagBackgroundColor = UIColor.white
        self.tagView.tagHighlightedBackgroundColor = UIColor.lightGray
        self.tagView.tagSelectedBackgroundColor = UIColor.lightGray
        self.tagView.textFont = UIFont.systemFont(ofSize: 15)
        
        // adding all tags of a photo
        self.tagView.delegate = self
        for tag in self.photo.tagsAsArray() {
            self.tagView.addTag(tag)
        }
        
    }
    


}














