//
//  StringExtensions.swift
//  Utdannet
//
//  Created by Narikbi on 12/22/16.
//  Copyright © 2016 All rights reserved.
//

import Foundation
import SwiftyAttributes

extension String {
    
    func notification() -> NSNotification.Name {
        return NSNotification.Name(rawValue: self)
    }

    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    func toNSNumber() -> NSNumber? {
        if let n = Float(self) {
            return NSNumber(value: n)
        }
        return nil
    }

    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }

    var trim : String {
        return self.trimmingCharacters(in: .whitespaces)
    }

    var length:Int {
        return self.characters.count
    }

    var withoutSpaces:String {
        return self.replacingOccurrences(of: " ", with: "")
    }

    var isNotEmpty:Bool {
        return !self.isEmpty
    }

    func ifEmpty(returnThis string:String) -> String {
        return self.isEmpty ? string : self
    }
}

extension String {
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    var uppercaseEachFirst: String {
        var words = [String]()
        for word in self.components(separatedBy: " ") {
            words.append(word.uppercaseFirst)
        }
        return words.joined(separator: " ")
    }
    var uppercaseFirst: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    var lowercaseAndUppercaseEachFirst: String {
        return self.lowercased().uppercaseEachFirst
    }
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

// MARK: Attributes

extension String {
    func strikethrough(color:UIColor) -> NSMutableAttributedString {
        return self.withAttributes([.strikethroughStyle(.styleSingle), .strikethroughColor(color)])
    }
}
