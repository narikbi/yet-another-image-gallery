//
//  AppDelegate.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        // Performing migrations in Realm if there any
        RealmHelper.performMigration()

        // config app UI
        self.configAppearanceAndSettings()
        
        // Switching to FeedVC
        self.gotoMain()
        
        self.window?.makeKeyAndVisible()

        return true
    }


    func gotoMain() {
        let vc = FeedViewController.instantiate()
        let nc = UINavigationController(rootViewController:vc)
        self.window?.rootViewController = nc
    }
    
    func configAppearanceAndSettings() {
        
        // Config keyboard manager for adding automatic insets for scroll view classes
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "↓"
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }

}

