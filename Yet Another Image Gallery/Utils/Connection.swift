//
//  Connection.swift
//  Utdannet
//
//  Created by Narikbi on 10/3/16.
//  Copyright © 2016 All rights reserved.
//

import Foundation
import ReachabilitySwift
import UIKit

class Connection {
    class func isReachable() -> Bool {
        let r = Reachability()!
        return r.currentReachabilityStatus == .reachableViaWiFi || r.currentReachabilityStatus == .reachableViaWWAN
    }

    static func isNotReachable() -> Bool {
        return !isReachable()
    }

    static func isReachableM() -> Bool {
        if self.isReachable() {
            return true
        }
        alertNoInternet()
        return false
    }

    static func isNotReachableM() -> Bool {
        return !isReachableM()
    }

    static func alertNoInternet() {
        UIApplication.shared.keyWindow?.rootViewController?.alert(title: "No internet connection", message: "Please check your connection and try again")
    }
}
