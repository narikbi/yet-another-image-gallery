//
// Created by Narikbi on 12/12/16.
// Copyright (c) 2016 All rights reserved.
//

import UIKit
import SnapKit
import SwiftyButton
import ChameleonFramework

var viewLoaderSettings = [UIView:AnyObject]()

extension UIView {

    static let noContentViewTag = 404

    func showNoContent(title:String?, message:String?, iconName:String? = nil, buttonTitle:String? = nil, buttonIcon:String? = nil, target:Any? = nil, action:Selector? = nil, backgroundColor:UIColor? = nil, animateImage:Bool = false) {

        var b:UIButton? = nil

        if let buttonTitle = buttonTitle {
            let button = FlatButton()
            button.setTitle(buttonTitle, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            button.color = UIColor.black
            button.highlightedColor = UIColor.lightGray
            button.cornerRadius  = 3
            button.setInsets(horizontal: 15, vertical: 8)

            b = button
        } else if let buttonIcon = buttonIcon {
            b = UIButton.button(icon: buttonIcon, sideInset: 10)
        }

        if let b = b, let action = action, let target = target {
            b.addTarget(target, action: action, for: .touchUpInside)
        }

        showNoContent(title: title, message: message, iconName: iconName, buttonView: b, backgroundColor: backgroundColor, animateImage:animateImage)
    }

    func showNoContent(title:String?, message:String?, iconName:String? = nil, buttonView:UIView? = nil, backgroundColor:UIColor? = nil, animateImage:Bool = false) {

        removeNoContentView()

        let container = UIView()
        container.tag = UIView.noContentViewTag
        self.addSubview(container)
        container.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.75)
            make.center.equalToSuperview()
        }

        if let color = backgroundColor {
            container.backgroundColor = color

            if self.isKind(of: UITableView.self) {
                viewLoaderSettings[self] = (self as! UITableView).separatorColor
                (self as! UITableView).separatorColor = color
            }
        }

        var aboveView : UIView? = nil
        if let icon = iconName {
            let iv = UIImageView(image: UIImage(named: icon))
            container.addSubview(iv)
            iv.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.centerX.equalToSuperview()
            })
            aboveView = iv

            if animateImage {
                animateView(imageView: aboveView!)
            }
        }

        var titleLabel: UILabel?
        if let title = title {
            titleLabel = UILabel()
            titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            titleLabel?.textColor = UIColor.RGB(hex: "91A4AD")
            titleLabel?.numberOfLines = 0
            titleLabel?.lineBreakMode = .byWordWrapping
            titleLabel?.textAlignment = .center
            titleLabel?.text = title
            container.addSubview(titleLabel!)
            titleLabel?.setLetterSpacing(value: -0.5)
            titleLabel?.snp.makeConstraints { make in
                if let view = aboveView {
                    make.top.equalTo(view.snp.bottom).offset(20)
                } else {
                    make.top.equalToSuperview()
                }
                make.left.right.equalToSuperview()
            }
            aboveView = titleLabel
        }

        var messageLabel: UILabel?
        if let message = message {
            messageLabel = UILabel()
            messageLabel?.font = UIFont.systemFont(ofSize: 13)
            messageLabel?.textColor = UIColor.RGB(hex: "91A4AD")
            messageLabel?.numberOfLines = 0
            messageLabel?.lineBreakMode = .byWordWrapping
            messageLabel?.textAlignment = .center
            messageLabel?.text = message
            container.addSubview(messageLabel!)
            messageLabel?.setLetterSpacing(value: -0.5)
            messageLabel?.snp.makeConstraints { make in
                if let view = aboveView {
                    make.top.equalTo(view.snp.bottom).offset(5)
                } else {
                    make.top.equalToSuperview()
                }
                make.left.right.equalToSuperview()
            }
        }

        if let button = buttonView {
            container.addSubview(button)

            if let label = messageLabel {
                button.snp.makeConstraints { make in
                    make.top.equalTo(label.snp.bottom).offset(15)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview()
                }
            } else {
                button.snp.makeConstraints { make in
                    make.top.bottom.equalToSuperview()
                    make.centerX.equalToSuperview()
                }
            }
        } else {
            if let label = messageLabel {
                label.snp.makeConstraints { make in
                    make.bottom.equalToSuperview()
                }
            }
        }
    }

    func removeNoContentView() {
        if let oldView = self.viewWithTag(UIView.noContentViewTag) {
            oldView.removeFromSuperview()

            if self.isKind(of: UITableView.self) {
                if let color = viewLoaderSettings[self] {
                    (self as! UITableView).separatorColor = color as? UIColor
                    viewLoaderSettings[self] = nil
                }
            }
        }
    }

    func animateView(imageView:UIView) {
        UIView.animate(withDuration: 1.0, animations: {
            imageView.transform = imageView.transform.rotated(by: CGFloat(Double.pi))
        }, completion: { b in
            if let _ = self.viewWithTag(UIView.noContentViewTag) {
                self.animateView(imageView: imageView)
            }
        })
    }

}
