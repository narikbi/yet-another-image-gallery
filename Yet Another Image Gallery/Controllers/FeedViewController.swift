//
//  FeedViewController.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit

// FeedViewController is main UIViewController subclass to be showed when app is launched
// Shows the list of `Photo` objects within tableView list
//

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    // main UITableView class instance
    @IBOutlet weak var tableView: UITableView!
    
    // control for refreshing the page
    var refreshControl:UIRefreshControl!
    
    // Search bar that used to be type tags
    var searchBar:UISearchBar!
    
    // Array of `Photo` objects to be listed
    var photos:[Photo] = []
    
    
    // Initalizing UIViewController from the main storyboard using StoryboardID
    // MARK: init
    static func instantiate() -> FeedViewController {
        return UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
    }
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Getting all cached photos from Realm
        photos = Photo.allPhotos()
        
        self.configUI()
        self.loadPhotos()
        self.addObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: Load photos
    
    func loadPhotos() {
        
        // Checks internet connection and shows message if there any problem
        if Connection.isNotReachableM() {
            return
        }
        
        if !refreshControl.isRefreshing {
            self.showSV()
        }
        
        PhotoApi.photos(text: self.searchBar.text!, success: { [unowned self] (arr) in
            self.hideSV()
            self.refreshControl.endRefreshing()
            
            self.photos = arr!
            self.tableView.reloadData()
            
            self.configNoContent()
            self.scrollToTop()
            
        }) { (message) in
            self.hideSV()
            self.refreshControl.endRefreshing()
            self.removeNoContentView()
            self.configNoContent()
        }
    }
    
    // MARK: Observers
    
    func addObservers() {
        NotificationsHelper.observe(target: self, selector: #selector(FeedViewController.tagSelected(notification:)), name: kNotificationTagSelected)
    }
    
    // Notification observer for selected tag
    func tagSelected(notification:Notification) {
        if let dict = notification.object as? NSDictionary {
            if let tag = dict["tag"] as? String {
                self.searchBar.text = tag
                self.loadPhotos()
            }
        }
    }
    
    // MARK: tableView Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableCell") as! PhotoTableCell
        let photo = photos[indexPath.row]
        cell.setPhoto(photo)
        
        return cell
    }
    
    //MARK: tableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let vc = PhotoViewController.instantiate()
        vc.photo = self.photos[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // Since PhotoTableCell's height is calculated by the autolayout returning UITableViewAutomaticDimension
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    // MARK: searchBar delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.loadPhotos()
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        if (searchBar.text?.length)! > 0 {
            searchBar.text = "";
            self.loadPhotos()
        }
    }

    // Showing message if there no content to show
    // and removing one if there is any
    func configNoContent() {
        if (self.photos.count == 0) {
            self.showNoContent(title: "No photos found", message: "Please, try again", iconName: nil)
        } else {
            self.removeNoContentView()
        }

    }
    
    // Scrolling to top when new photos comes out
    func scrollToTop() {
        if self.tableView.numberOfRows(inSection: 0) > 0 {
            self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    // Making all configuration with UI
    // MARK: configUI
    
    func configUI() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: "PhotoTableCell", bundle: nil), forCellReuseIdentifier: "PhotoTableCell")
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        
        searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .minimal
        self.navigationItem.titleView = searchBar;
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadPhotos), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
    }
    
}






