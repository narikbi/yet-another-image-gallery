//
// Created by Narikbi on 3/27/17.
// Copyright (c) 2017. All rights reserved.
//

import Foundation
import ChameleonFramework

extension UIColor {
    static func RGB(hex:String) -> UIColor {
        return UIColor.init(hexString:hex)!
    }
}
