//
//  LabelExtensions.swift
//  Utdannet
//
//  Created by Narikbi on 10/11/16.
//  Copyright © 2016 All rights reserved.
//

import UIKit


extension UILabel {
    
    func setLetterSpacing(value:CGFloat) {
        let attributedString = mutableAttributedString()
        attributedString.addAttributes([NSKernAttributeName : value], range: NSMakeRange(0, attributedString.length))
        self.attributedText = attributedString
    }
    
    private func mutableAttributedString() -> NSMutableAttributedString {
        if let string = self.attributedText {
            return NSMutableAttributedString(attributedString: string)
        }
        
        return NSMutableAttributedString.init()
    }
    
}
