//
// Created by Narikbi on 12/12/16.
// Copyright (c) 2016 All rights reserved.
//

import UIKit
import SnapKit

extension UIViewController {

    func showNoContent(title:String?, message:String?, iconName:String?, buttonTitle:String? = nil, buttonIcon:String? = nil, action:Selector? = nil, animateImage:Bool = false) {
        self.view.showNoContent(title: title, message: message, iconName: iconName, buttonTitle:buttonTitle, buttonIcon: buttonIcon, target:self, action: action, animateImage: animateImage)
    }

    func removeNoContentView() {
        self.view.removeNoContentView()
    }
}
