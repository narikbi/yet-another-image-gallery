//
//  PhotoApi.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias SuccessArray = (_ data: [Photo]?) -> Void

/// `PhotoApi` extends `APIClient` to send all requests related to Flickr API Photo requests
//

class PhotoApi : APIClient {

    /// Get photos from public Flickr API
    /// - parameters text - A tag to be searched
    /// - parameter success - A block returns array of `Photo` objects
    /// - parameter failure - A block to be executed when request failed
    
    static func photos(text:String, success:@escaping SuccessArray, failure:@escaping Failure) {
        
        var params = [String:Any?]();
        
        if text.length > 0 {
            params["tags"] = text
        }
        params["format"] = "json"
        
        // spacial key for getting json withour unuseful keywords before json starts
        params["nojsoncallback"] = "1"
        
        self.GET(path: "photos_public.gne", params: params as! [String : String], success: { (response) in
            
            guard let response = response else {return}
            
            let json = JSON(response)
            let items = json["items"].arrayValue
            var photos:[Photo] = []
            
            Photo.removePhotos()
            for item in items {
                let photo = Photo.addOrUpdatePhoto(json: item)
                photos.append(photo)
            }
            
            success(photos)
            
        }, failure: failure)
        
    }
    
    
    
    
    
}













