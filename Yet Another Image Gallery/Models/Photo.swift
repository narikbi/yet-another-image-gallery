//
//  Photo.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import SwiftDate

/// `Photo` represent a photo that we're fetching from Flickr API
/// It extends `Object` class in order to be cached in Realm database
///

class Photo: Object {
    
    dynamic var title = ""
    dynamic var link = ""
    dynamic var media = ""
    dynamic var dateTaken:Date?
    dynamic var descriptionText = ""
    dynamic var published:Date?
    dynamic var author = ""
    dynamic var authorId:Int = 0
    dynamic var tags = ""

    
    // assuming that `link` parameter is unique for every photo in Realm
    
    override static func primaryKey() -> String? {
        return "link"
    }

    
    /// Updates `Photo` object into database if it exists in Realm,
    /// otherwise inserts new objects. 
    /// 
    /// - parameters json: `SwiftyJSON` class instanse
    /// - returns: updated or created `Photo` object instance
    
    static func addOrUpdatePhoto(json:JSON) -> Photo {
        
        let link = json["link"].stringValue
        let oldObject = Photo.photo(link: link)
        
        var object:Photo
        if oldObject != nil {
            object = oldObject!
        } else {
            object = Photo()
            object.link = link
        }
        
        let realm = try! Realm()
        try! realm.write {
            
            object.title = json["title"].stringValue
            object.media = json["media"]["m"].stringValue
            object.descriptionText = json["description"].stringValue
            object.author = json["author"].stringValue
            object.authorId = json["author_id"].intValue
            object.tags = json["tags"].stringValue
            
            // Date
            let dateTaken = json["date_taken"].stringValue
            if dateTaken.length > 0 {
                
                let dateFormatter = DateFormatter()
                let enUSPosixLocale = NSLocale(localeIdentifier: "en_US_POSIX")
                dateFormatter.locale = enUSPosixLocale as Locale!
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                object.dateTaken = dateFormatter.date(from: dateTaken)
            }

            let published = json["published"].stringValue
            if published.length > 0 {
                let d = published.date(format: DateFormat.custom("yyyy-MM-dd'T'HH:mm:ssZ"))
                object.published = d?.absoluteDate
            }
            
            if oldObject == nil {
                realm.add(object)
            }
        }

        return object
    }
    
    
    /// searching photo by `link` parameter
    /// - parameters link: String object
    /// - returns: found `Photo` object instance or `nil` otherwise
   
    static func photo(link:String) -> Photo? {
        let realm = try! Realm()
        return realm.object(ofType: Photo.self, forPrimaryKey: link)
    }
    
    
    /// Initially Flickr API gives a photo with low quality, 
    /// so in order to get better quality, we're just removing `_m` part from the link,
    /// and it just works! :)
    /// for example, `cat_m.jpg` -> `cat.jpg`
    ///
    /// - returns: fullsize photo link
    
    func fullsizePhoto() ->String {
        let media = self.media.replacingOccurrences(of: "_m", with: "")
        return media
    }
    
    
    /// Initially Flickr API gives author name sticked with email (that is mostly unuseful by the way)
    /// so we're just separating components and removing extra  `(`, `)` charaters from author name
    /// for example, `nobody@flickr.com (\"Batman\")` -> `Batman`
    ///
    /// - returns: author name
    
    func formattedUsername() -> String {
        var names = [String]()
        if author.isNotEmpty {
            names = author.components(separatedBy: " ")
        }
        
        var name = ""
        if names.count > 0 {
            name = names.last!
            name = name.replacingOccurrences(of: "(\"", with: "")
            name = name.replacingOccurrences(of: "\")", with: "")
        }
        
        return name
    }
    
    /// - returns: Formatted, human readable `dateTaken` String
    func formattedDateTaken() -> String {
        let d = self.dateTaken?.string(custom: "dd MMM, yyyy")
        return d!

    }
    
    /// Initially Flickr API gives tags as string separated by ` ` space,
    /// so we're just separating components and removing extra spaces
    /// for example, `cats dogs football` -> [`cats`, `dogs`, `football`]
    ///
    /// - returns: tags array
    
    func tagsAsArray() -> [String] {
        var tags = [String]()
        if self.tags.isNotEmpty {
            tags = self.tags.components(separatedBy: " ")
        }
        return tags
    }
}

extension Photo {

    
    /// - returns: All `Photo` objects sorted by `published`
    static func allPhotos() -> [Photo] {
        let realm = try! Realm()
        let results = realm.objects(Photo.self).sorted(byKeyPath: "published", ascending: false) as Results <Photo>
        return Array(results)
    }
    
    /// removes all `Photo` objects from database
    static func removePhotos() {
        let realm = try! Realm()
        try! realm.write {
            for object in realm.objects(Photo.self) {
                realm.delete(object)
            }
        }
    }

    
}

























