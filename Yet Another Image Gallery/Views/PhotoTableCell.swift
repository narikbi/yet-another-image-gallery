//
//  PhotoTableCell.swift
//  Yet Another Image Gallery
//
//  Created by Narikbi on 21.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import UIKit
import DateToolsSwift

class PhotoTableCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.layer.cornerRadius = 15
        
    }

    func setPhoto(_ photo:Photo) {
        self.photoImageView.sd_setImage(with: URL(string: photo.fullsizePhoto()))
        self.avatarImageView.image = randomAvatar()
        self.titleLabel.text = photo.title
        self.dateLabel.text = photo.published?.timeAgoSinceNow.uppercased()
        self.usernameLabel.text = photo.formattedUsername()
    }
    
    func randomAvatar()-> UIImage {
        let random = Int(arc4random_uniform(3))
        let imgName = "ava_\(random+1)";
        return UIImage(named: imgName)!
    }
    
}
