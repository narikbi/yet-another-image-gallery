//
// Created by Narikbi on 3/3/17.
// Copyright (c) 2017 All rights reserved.
//

import Foundation

class NotificationsHelper {

    // MARK: Local

    static func post(name:String, object:Any? = nil) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: object)
    }

    static func observe(target:Any, selector:Selector, name:String) {
        NotificationCenter.default.addObserver(target, selector: selector, name: Notification.Name(rawValue: name), object: nil)
    }


}
