//
//  RealmHelper.swift
//  Utdannet
//
//  Created by Narikbi on 11/29/16.
//  Copyright © All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper {

    static let version:UInt64 = 1
    
    static func performMigration() {
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration( schemaVersion: version, migrationBlock: { migration, oldSchemaVersion in
            if (oldSchemaVersion < version) {
            }
        })

        if Constants.kDEBUG, let path = Realm.Configuration.defaultConfiguration.fileURL?.absoluteString {
            print("\((path as String).replacingOccurrences(of: "file://", with: ""))")
        }
    }
}
