//
//  Yet_Another_Image_GalleryUITests.swift
//  Yet Another Image GalleryUITests
//
//  Created by Narikbi on 20.07.17.
//  Copyright © 2017 app.yaig.com. All rights reserved.
//

import XCTest

class Yet_Another_Image_GalleryUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    // test for searching a photo with particular tag
    func testTagSearch() {
        let app = XCUIApplication()
        self.searchCatAndTap(app)
    }
    
    
    // Test for photo saving. Works only after photo saving is allowed by system.
    func testPhotoSaving() {
        let app = XCUIApplication()
        self.searchCatAndTap(app)
        
        // tapping action button on navigation bar
        app.navigationBars["Info"].buttons.element(boundBy: 1).tap()
        app.sheets["Action"].buttons["Save photo"].tap()
        
        sleep(3)
    }
    
    // Test for opening a photo in Safari
    func testPhotoInBrowser() {
        let app = XCUIApplication()
        self.searchCatAndTap(app)
        
        // tapping action button on navigation bar
        app.navigationBars["Info"].buttons.element(boundBy: 1).tap()
        app.sheets["Action"].buttons["Open photo in Safari"].tap()
        
        sleep(3)
    }
    
    // Test for sharing a photo by email
    func testEmailComposer() {
        let app = XCUIApplication()
        self.searchCatAndTap(app)
        
        // tapping action button on navigation bar
        app.navigationBars["Info"].buttons.element(boundBy: 1).tap()
        app.sheets["Action"].buttons["Send by email"].tap()
        
        sleep(3)
    }
    
    
    // helper for searching cats :)
    func searchCatAndTap(_ app : XCUIApplication) {
        sleep(3)
        
        // typing searchBar text
        let textField = app.searchFields["Search"]
        textField.tap()
        textField.typeText("Cat")
        // pressing `Search` button on keyboard
        textField.typeText("\n")
        
        sleep(3)
        
        // tapping on the first cell
        let tablesQuery = app.tables
        tablesQuery.cells.element(boundBy: 0).tap()
        
        sleep(4)
        
    }
    
    
    
}
