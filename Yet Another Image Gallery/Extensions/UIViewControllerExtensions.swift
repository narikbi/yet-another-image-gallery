//
//  UIViewControllerExtensions.swift
//  Utdannet
//
//  Created by Narikbi on 10/31/16.
//  Copyright © 2016 All rights reserved.
//

import UIKit

extension UIViewController {

    // MARK: - Actions

    func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    func dismissController() {
        self.dismiss(animated: true)
    }

    // MARK: Global

    func appDelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }

    func presentedViewControllerOrViewController() -> UIViewController {
        if let modal = self.presentedViewController {
            return modal
        }
        return self
    }

    // MARK: NavigationBar

    func setNavigationBack(iconName:String = "icon-back") {
        let button = UIButton.button(icon: iconName, top:10, left:2, bottom:10, right:15)
        button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        button.showsTouchWhenHighlighted = true
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item;
    }
    
    func setNavigationButton(icon:String, action:Selector, insets:UIEdgeInsets = .zero, atLeft:Bool = false) {
        let button = UIButton.button(icon:icon, top:insets.top, left:insets.left, bottom:insets.bottom, right:insets.right)
        button.addTarget(self, action: action, for: .touchUpInside)
        let barItem = UIBarButtonItem.init(customView: button)
        
        if atLeft {
            self.navigationItem.leftBarButtonItem = barItem
        } else {
            self.navigationItem.rightBarButtonItem = barItem
        }
    }
    
    func navigationControllerLeaveLastController() {
        let array = self.navigationController?.viewControllers
        if let a = array, a.count > 0 {
            self.navigationController?.viewControllers = [(a.last)!]
        }
    }

    func enableNavigationRightBarButton(enable:Bool = true) {
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = enable
        }
    }

    func setNavigationBarColor(color:UIColor) {
        if let nc = self.navigationController {
            nc.navigationBar.barTintColor = color
        }
    }

    func setNavigationTitleColor(color:UIColor) {
        if let nc = self.navigationController {
            nc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: color]
        }
    }

    // MARK: Keyboard

    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: - Alerts
    
    func alertTryAgain() {
        alert(title:"Error", message:"Please try again")
    }
    
    func alert(message:String) {
        alert(title: nil, message: message)
    }

    func alert(title:String?, message:String?, cancelButton:String = "OK", actionButton:String? = nil, handler: ((Void) -> (Void))? = nil, cancelHandler: ((Void) -> (Void))? = nil) {
        let alert = UIAlertController(title: title ?? "", message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelButton, style: .cancel) { _ in
            if let h = cancelHandler {
                h()
            }
        })
        if let ab = actionButton {
            alert.addAction(UIAlertAction(title: ab, style: .default) { action in
                if let h = handler {
                    h()
                }
            })
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Action Sheet
    func actionSheet(title: String?, message:String?, cancelButton:String = "Cancel", actionButton:String? = nil, handler: ((Void) -> (Void))? = nil, cancelHandler: ((Void) -> (Void))? = nil) {
        let actionSheet = UIAlertController(title: title ?? "", message: message, preferredStyle: .actionSheet)
        
        if let action = actionButton {
            actionSheet.addAction(UIAlertAction(title: action, style: .destructive, handler: { (action) -> Void in
                if let h = handler {
                    h()
                }
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: cancelButton, style: .cancel) { _ in
            if let h = cancelHandler {
                h()
            }
        })
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func actionSheet(title: String?, message:String?, cancelButton:String = "Cancel", actionButton:String? = nil, otherButtons:[String], handler: ((Void) -> (Void))? = nil, cancelHandler: ((Void) -> (Void))? = nil) {
        let actionSheet = UIAlertController(title: title ?? "", message: message, preferredStyle: .actionSheet)
        
        if let action = actionButton {
            actionSheet.addAction(UIAlertAction(title: action, style: .destructive, handler: { (action) -> Void in
                if let h = handler {
                    h()
                }
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: cancelButton, style: .cancel) { _ in
            if let h = cancelHandler {
                h()
            }
        })
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: UITabBar

    func setTabBarItem(title:String?, imageName:String, selectedImageName:String? = nil, titlePositionAdjustment position:UIOffset = UIOffset(horizontal: 0, vertical: 0)) -> UITabBarItem {
        let image = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal)
        var selectedImage : UIImage? = nil
        if let si = selectedImageName {
            selectedImage = UIImage(named: si)
        }
        let barItem:UITabBarItem = UITabBarItem(title: title ?? "", image: image, selectedImage: selectedImage)
        barItem.titlePositionAdjustment = position
        self.tabBarItem = barItem

        return barItem
    }
}







