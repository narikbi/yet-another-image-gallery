//
// Created by Narikbi on 4/12/17.
// Copyright (c) 2017 All rights reserved.
//

import Foundation
import SVProgressHUD

extension UIView {

    func showSV(with status:String = "") {
        if status.length > 0 {
            SVProgressHUD.show(withStatus: status)
        } else {
            SVProgressHUD.show()
        }
    }

    func successSV(message:String = "") {
        SVProgressHUD.showSuccess(withStatus: message)
    }

    func hideSV() {
        SVProgressHUD.dismiss()
    }

}

extension UIViewController {
    func showSV(with status:String = "") {
        self.view.showSV(with: status)
    }

    func successSV(message:String = "") {
        self.view.successSV(message: message)
    }

    func hideSV() {
        SVProgressHUD.dismiss()
    }
}
